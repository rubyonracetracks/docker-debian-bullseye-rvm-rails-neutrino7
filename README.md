[![pipeline status](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-rvm-rails-neutrino7/badges/main/pipeline.svg)](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-rvm-rails-neutrino7/-/commits/main) 

# Docker Debian Bullseye - RVM - Rails - Neutrino 7

This repository is used for building the RVM Rails Neutrino 7 Debian Bullseye Docker image for [Ruby on Racetracks](https://www.rubyonracetracks.com/).

## Name of This Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-bullseye-rvm-rails-neutrino7](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-rvm-rails-neutrino7/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-bullseye-min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-bullseye-min-rvm/container_registry)

## What's Added
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems used in the Rails Neutrino 7 Minimal App.
* Bundler
* The mailcatcher gem

## What's the Point?
This Docker image is used for running the Rails Neutrino 7 Minimal App.  The process of getting started is MUCH faster in a development environment that comes with the correct versions of Ruby and the correct versions of certain gems already pre-installed.  The rails, pg, nokogiri, and ffi gems take a long time to install.

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-common/blob/main/FAQ.md).
